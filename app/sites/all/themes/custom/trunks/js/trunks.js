(function ($) {
    $(document).ready(function(){
    
    // sweet menu color animations
        $('.menu-nav-container.main li a').each(function(){
            var originalColor = $(this).css('color');
            $(this).mouseover(function(){
                $(this)
                    .animate({color: '#fff'})
                    .closest('li').animate({backgroundColor: '#ad5757'}, 150);
            }).mouseout(function(){
                $(this)
                    .animate({color: originalColor})
                    .closest('li').animate({backgroundColor: 'rgba(0,0,0,0)'}, 150);
            });
        });       
    });
}(jQuery));